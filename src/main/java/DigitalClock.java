import javax.swing.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.TimerTask;
import java.util.Timer;

public class DigitalClock {
    public static void main(String[] args) {

        JFrame frame = new JFrame();
        frame.setTitle("Jon's Clock");
        frame.setSize(400, 100);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setVisible(true);

        JLabel label = new JLabel("", SwingConstants.CENTER);
        label.setSize(400,100);
        label.setFont(label.getFont().deriveFont(25f));
        frame.add(label);

        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                LocalDateTime dateTime = LocalDateTime.now();
                DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MMMM dd, yyyy, HH:mm:ss");
                String dateStamp = dtf.format(dateTime);
                label.setText(dateStamp);
            }
        };

        Timer timer = new Timer();
        timer.scheduleAtFixedRate(timerTask, 0, 1000);
    }
}
